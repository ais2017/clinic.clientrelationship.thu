package db;

import logic.Patient;
import org.springframework.stereotype.Service;

import java.util.Collection;

public interface PatientInteface {

    Patient getOne(Long id);

    Collection<Patient> getAll();

    Patient save(Patient patient);

    void delete(Long id);

    void update(Long id, Patient patient);

    Patient getByPolicyNumber(String policyNumber);

    Collection<Patient> getAllBlocked();

    Patient generate();
}

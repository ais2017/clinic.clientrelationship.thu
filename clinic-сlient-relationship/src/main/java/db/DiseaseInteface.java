package db;

import logic.AvailableAdmissionTime;
import logic.DiseaseHistory;

import java.util.Collection;

public interface DiseaseInteface {

    DiseaseHistory getOne(Long id);

    Collection<DiseaseHistory> getAll();

    void delete(Long id);

    DiseaseHistory update(Long id, DiseaseHistory diseaseHistory);

    DiseaseHistory save (DiseaseHistory diseaseHistory);
}

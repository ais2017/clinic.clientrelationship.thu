package db;

import logic.AvailableAdmissionTime;

import java.util.Collection;

public interface AvailableAdmissionTimeInterface {

    AvailableAdmissionTime getOne(Long id);

    Collection<AvailableAdmissionTime> getAll();

    void delete(Long id);

    void update(Long id);

    AvailableAdmissionTime save (AvailableAdmissionTime availableAdmissionTime);

    AvailableAdmissionTime generate(String startTime, String endTime);
}

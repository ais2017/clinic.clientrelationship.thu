package db;

import logic.TimeTable;

import java.util.Collection;
import java.util.Date;

public interface TimeTableInterface {

    TimeTable getOne(Long id);

    Collection<TimeTable> getAll();

    void delete(Long id);

    TimeTable update(Long id, TimeTable timeTable);

    TimeTable save(TimeTable object);

    TimeTable generate(Date date);
}

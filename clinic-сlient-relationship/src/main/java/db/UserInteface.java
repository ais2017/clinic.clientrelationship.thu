package db;

import authority.User;

public interface UserInteface {

    User getByLogin(String login);

    void delete(Long id);

    void save(User object);
}

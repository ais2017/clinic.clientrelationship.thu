package db.impl.postgres;

import db.OrderedMedicalServiceInterface;
import logic.Doctor;
import logic.MedicalService;
import logic.OrderedMedicalService;

import javax.persistence.NoResultException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * Created by anna on 14.12.2017.
 */
public class OrderedMedicalServiceRepository implements OrderedMedicalServiceInterface {

    public OrderedMedicalService getOne(Long id) {
        Connection c = null;
        Statement stmt = null;
        OrderedMedicalService orderedMedicalService = new OrderedMedicalService();
        try {
            c = DBConnection.getDBConnection();
            c.setAutoCommit(false);
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM OrderedMedicalService WHERE id = " + id );
            while ( rs.next() ) {
                orderedMedicalService = fillOrderedMedicalService(rs, orderedMedicalService);
            }
            rs.close();
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
        }
        if (orderedMedicalService.getId() == null) {
            throw new NoResultException();
        }
        return orderedMedicalService;
    }

    public Collection<OrderedMedicalService> getAll() {
        return null;
    }

    public void delete(Long id) {
    }
    public OrderedMedicalService update(Long id, OrderedMedicalService orderedMedicalService) {
        orderedMedicalService.setId(id);
        Connection c = null;
        Statement stmt = null;
        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "UPDATE OrderedMedicalService SET admissionDate = ?, patientId = ?," +
                " isPaid = ?, payDate = ?, diseaseHistoryId = ?, medicalServiceId = ? where id = ?";

        try {
            dbConnection = DBConnection.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement = fillForUpdate(preparedStatement, orderedMedicalService);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {
            DBConnection.closeConnection(preparedStatement, c);
        }
        return orderedMedicalService;
    }

    public OrderedMedicalService generate() {
        OrderedMedicalService orderedMedicalService = new OrderedMedicalService();
        return orderedMedicalService;
    }

    @Override
    public OrderedMedicalService save(OrderedMedicalService orderedMedicalService) {
        Connection c = null;
        Statement stmt = null;
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO OrderedMedicalService "
                + "(admissionDate, patientId, isPaid, payDate, diseaseHistoryId, medicalServiceId) VALUES"
                + "(?,?,?,?,?,?)";
        try {
            c = DBConnection.getDBConnection();
            preparedStatement = c.prepareStatement(insertTableSQL);
            preparedStatement = fillForUpdate(preparedStatement, orderedMedicalService);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {
            DBConnection.closeConnection(preparedStatement, c);
        }
        return orderedMedicalService;
    }


    @Override
    public Collection<OrderedMedicalService> getAllByPatientPolicyNumber(String policyNumber) {
        return getAll();
    }

    private OrderedMedicalService fillOrderedMedicalService(ResultSet rs, OrderedMedicalService orderedMedicalService) throws SQLException {
        orderedMedicalService.setId(rs.getLong("id"));
        orderedMedicalService.setAdmissionDate(rs.getTimestamp("admissionDate"));
        orderedMedicalService.setPatientId(rs.getLong("patientId"));
        orderedMedicalService.setPaid(rs.getBoolean("isPaid"));
        orderedMedicalService.setPayDate(rs.getTimestamp("payDate"));
        orderedMedicalService.setDiseaseHistoryId(rs.getLong("diseaseHistoryId"));
        orderedMedicalService.setMedicalServiceId(rs.getLong("medicalServiceId"));
        return orderedMedicalService;
    }

    private PreparedStatement fillForUpdate(PreparedStatement preparedStatement, OrderedMedicalService orderedMedicalService) throws SQLException {
        preparedStatement.setTimestamp(1, new Timestamp(orderedMedicalService.getAdmissionDate().getTime()));
        preparedStatement.setLong(2, orderedMedicalService.getPatientId());
        preparedStatement.setBoolean(3, orderedMedicalService.isPaid());
        preparedStatement.setTimestamp(4, new Timestamp(orderedMedicalService.getPayDate().getTime()));
        preparedStatement.setLong(5, orderedMedicalService.getDiseaseHistoryId());
        preparedStatement.setLong(6, orderedMedicalService.getMedicalServiceId());
        return preparedStatement;
    }
}

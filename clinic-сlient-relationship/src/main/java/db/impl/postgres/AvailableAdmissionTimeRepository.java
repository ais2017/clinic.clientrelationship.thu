package db.impl.postgres;

import db.AvailableAdmissionTimeInterface;
import logic.AvailableAdmissionTime;

import javax.persistence.NoResultException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by anna on 14.12.2017.
 */
public class AvailableAdmissionTimeRepository implements AvailableAdmissionTimeInterface {

    private static AtomicLong id = new AtomicLong(0);

    public static Map<Long, AvailableAdmissionTime> availableAdmissionTimeMap = new HashMap<Long, AvailableAdmissionTime>();

    public AvailableAdmissionTime getOne(Long id) {
        if (availableAdmissionTimeMap.get(id) == null) {
            throw new NoResultException();
        }
        return availableAdmissionTimeMap.get(id);
    }

    public Collection<AvailableAdmissionTime> getAll() {
        return availableAdmissionTimeMap.values();
    }

    public void delete(Long id) {
        if (availableAdmissionTimeMap.get(id) == null) {
            throw new NoResultException();
        }
        availableAdmissionTimeMap.remove(id);
    }
    public void update(Long id) {

    }

    public AvailableAdmissionTime generate(String startTime, String endTime) {
        return new AvailableAdmissionTime(null, startTime, endTime);
    }

    @Override
    public AvailableAdmissionTime save(AvailableAdmissionTime availableAdmissionTime) {
        long key = id.incrementAndGet();
        availableAdmissionTime.setId(key);
        availableAdmissionTimeMap.put(key, availableAdmissionTime);
        return availableAdmissionTime;
    }

    public static void resetId() {
        id.set(0);
    }
}

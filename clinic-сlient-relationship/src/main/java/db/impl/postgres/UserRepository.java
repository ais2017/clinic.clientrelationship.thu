package db.impl.postgres;

import authority.User;
import db.UserInteface;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class UserRepository implements UserInteface {

    private static AtomicLong id = new AtomicLong(0);

    public static Map<Long, User> users = new HashMap<Long, User>();

    @Override
    public User getByLogin(String login) {
        return users.values().stream().filter(user -> user.getLogin().equals(login)).findFirst().orElse(null);
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public void save(User object) {
        long key = id.incrementAndGet();
        object.setId(key);
        users.put(key, object);
    }
}

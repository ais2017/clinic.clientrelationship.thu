package db.impl.postgres;

import db.DoctorInterface;
import logic.Doctor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.persistence.NoResultException;
import javax.print.Doc;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;


/**
 * Created by anna on 09.12.2017.
 */
public class DoctorRepository implements DoctorInterface {

    public Doctor getOne(Long id) {
        Connection c = null;
        Statement stmt = null;
        Doctor doctor = new Doctor();
        try {
            c = DBConnection.getDBConnection();
            c.setAutoCommit(false);
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Doctor WHERE id = " + id );
            while ( rs.next() ) {
                doctor.setId(rs.getLong("id"));
                doctor.setName(rs.getString("name"));
                doctor.setSurname(rs.getString("surname"));
                doctor.setSpecialization(rs.getString("specialization"));
            }
            rs.close();
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
        }
        if (doctor.getId() == null) {
            throw new NoResultException();
        }
        return doctor;
    }

    public Collection<Doctor> getAll() {
        Connection c = null;
        Statement stmt = null;
        Doctor doctor = null;
        Collection<Doctor> result = new ArrayList<>();
        try {
            c = DBConnection.getDBConnection();
            c.setAutoCommit(false);

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Doctor");
            while ( rs.next() ) {
                doctor = new Doctor();
                doctor = fillDoctor(rs, doctor);
                result.add(doctor);
            }
            rs.close();
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
        }
        if (result.size() == 0) {
            throw new NoResultException();
        }
        return result;
    }

    public Doctor save(Doctor doctor) {
        Connection c = null;
        Statement stmt = null;
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO Doctor"
                + "(name, surname, patronymic, specialization) VALUES"
                + "(?,?,?,?)";

        try {
            c = DBConnection.getDBConnection();
            preparedStatement = c.prepareStatement(insertTableSQL);

            preparedStatement.setString(1, doctor.getName());
            preparedStatement.setString(2, doctor.getSurname());
            preparedStatement.setString(3, doctor.getPatronymic());
            preparedStatement.setString(4, doctor.getSpecialization());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {
            DBConnection.closeConnection(preparedStatement, c);
        }
        return doctor;
    }

    public void delete(Long id) {
    }

    public void update(Long id, Doctor doctor) {
        doctor.setId(id);
        Connection c = null;
        Statement stmt = null;
        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "UPDATE Doctor SET name = ?, surname = ?, patronymic = ?, specialization = ? where id = ?";

        try {
            dbConnection = DBConnection.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setString(1, doctor.getName());
            preparedStatement.setString(2, doctor.getSurname());
            preparedStatement.setString(3, doctor.getPatronymic());
            preparedStatement.setString(4, doctor.getSpecialization());
            preparedStatement.setLong(5, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {
           DBConnection.closeConnection(preparedStatement, c);
        }
    }


    public Doctor generate() {
        return new Doctor(null,"Name", "Surname", "Patronymic", "Specialization");
    }

    public Doctor getBySurname(String surname) {
        Connection c = null;
        Statement stmt = null;
        Doctor doctor = new Doctor();
        try {
            c = DBConnection.getDBConnection();
            c.setAutoCommit(false);
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Doctor WHERE surname = \'" + surname + "\'");
            while ( rs.next() ) {
               doctor = fillDoctor(rs, doctor);
            }
            rs.close();
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
        }
        if (doctor.getId() == null) {
            throw new NoResultException();
        }
        return doctor;
    }

    private Doctor fillDoctor(ResultSet rs, Doctor doctor) throws SQLException {
        doctor.setId(rs.getLong("id"));
        doctor.setName(rs.getString("name"));
        doctor.setSurname(rs.getString("surname"));
        doctor.setSpecialization(rs.getString("specialization"));
        return doctor;
    }

}


package db.impl.postgres;

import db.TimeTableInterface;
import logic.AvailableAdmissionTime;
import logic.TimeTable;

import javax.persistence.NoResultException;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by anna on 11.12.2017.
 */
public class TimeTableRepository implements TimeTableInterface {

    private static AtomicLong id = new AtomicLong(0);

    public static Map<Long, TimeTable> timeTableMap = new HashMap<Long, TimeTable>();

    public TimeTable getOne(Long id) {
        return null;
    }

    public Collection<TimeTable> getAll() {
        return null;
    }

    public void delete(Long id) {

    }
    public TimeTable update(Long id, TimeTable timeTable) {
        if (timeTableMap.get(id) == null) {
            throw new NoResultException();
        }
        timeTableMap.remove(id);
        timeTable.setId(id);
        timeTableMap.put(id, timeTable);
        return timeTable;
    }

    public TimeTable generate(Date date) {
        TimeTable timeTable = new TimeTable(null, date, new ArrayList<AvailableAdmissionTime>());
        return timeTable;
    }

    @Override
    public TimeTable save(TimeTable object) {
        long key = id.incrementAndGet();
        object.setId(key);
        timeTableMap.put(key, object);
        return object;
    }

    public static void resetId() {
        id.set(0);
    }
}

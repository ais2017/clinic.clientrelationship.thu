package db.impl.postgres;

import db.MedicalServiceInterface;
import logic.Doctor;
import logic.MedicalService;

import javax.persistence.NoResultException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by anna on 11.12.2017.
 */
public class MedicalServiceRepository implements MedicalServiceInterface {

    public MedicalService getOne(Long id) {
        Connection c = null;
        Statement stmt = null;
        MedicalService medicalService = new MedicalService();
        try {
            c = DBConnection.getDBConnection();
            c.setAutoCommit(false);
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM MedicalService WHERE id = " + id );
            while ( rs.next() ) {
                medicalService = fillMedicalService(rs, medicalService);
            }
            rs.close();
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
        }
        if (medicalService.getId() == null) {
            throw new NoResultException();
        }
        return medicalService;
    }

    public Collection<MedicalService> getAll() {
        Connection c = null;
        Statement stmt = null;
        MedicalService medicalService = null;
        Collection<MedicalService> result = new ArrayList<>();
        try {
            c = DBConnection.getDBConnection();
            c.setAutoCommit(false);

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM MedicalService");
            while ( rs.next() ) {
                medicalService = new MedicalService();
                medicalService = fillMedicalService(rs, medicalService);
                result.add(medicalService);
            }
            rs.close();
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
        }
        if (result.size() == 0) {
            throw new NoResultException();
        }
        return result;
    }

    public void delete(Long id) {

    }

    public void update(Long id) {

    }

    public MedicalService generate() {
        MedicalService medicalService = new MedicalService(null, "Name", 100, "description");
        return medicalService;
    }

    @Override
    public MedicalService save(MedicalService medicalService) {
        Connection c = null;
        Statement stmt = null;
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO MedicalService"
                + "(name, description, price, doctorId) VALUES"
                + "(?,?,?,?)";

        try {
            c = DBConnection.getDBConnection();
            preparedStatement = c.prepareStatement(insertTableSQL);

            preparedStatement.setString(1, medicalService.getName());
            preparedStatement.setString(2, medicalService.getDescription());
            preparedStatement.setInt(3, medicalService.getPrice());
            preparedStatement.setLong(4, medicalService.getDoctorId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {
            DBConnection.closeConnection(preparedStatement, c);
        }
        return medicalService;
    }

    private MedicalService fillMedicalService(ResultSet rs, MedicalService medicalService) throws SQLException {
        medicalService.setId(rs.getLong("id"));
        medicalService.setName(rs.getString("name"));
        medicalService.setDescription(rs.getString("description"));
        medicalService.setPrice(rs.getInt("price"));
        medicalService.setDoctorId(rs.getLong("doctorId"));
        return medicalService;
    }
}

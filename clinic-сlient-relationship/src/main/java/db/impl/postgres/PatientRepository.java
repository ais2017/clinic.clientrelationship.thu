package db.impl.postgres;

import db.PatientInteface;
import logic.DiseaseHistory;
import logic.Patient;

import javax.persistence.NoResultException;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * Created by anna on 11.12.2017.
 */
public class PatientRepository implements PatientInteface {

    private static AtomicLong id = new AtomicLong(0);

    public static Map<Long, Patient> patients = new HashMap<Long, Patient>();

    public Patient getOne(Long id) {
        return null;
    }

    public Collection<Patient> getAll() {
        return patients.values();
    }

    public Patient save(Patient patient) {
        long key = id.incrementAndGet();
        patient.setId(key);
        patients.put(key, patient);
        return patient;
    }

    public void delete(Long id) {
        if (patients.get(id) == null) {
            throw new NoResultException();
        }
        patients.remove(id);
    }

    public void update(Long id, Patient patient) {
        if (patients.get(id) == null) {
            throw new NoResultException();
        }
        patients.remove(id);
        patient.setId(id);
        patients.put(id, patient);
    }
    public Patient generate() {
        return new Patient(null, Long.toString(new Random().nextInt(999999999) + 999999), "Name", "Surname", "Patronymic",
                new ArrayList<DiseaseHistory>(), new ArrayList<>());
    }

    public static void resetId() {
        id.set(0);
    }

    public Patient getByPolicyNumber(String policyNumber) {
        return patients.values().stream().filter(patient -> patient.getPolicyNumber().equals(policyNumber)).findFirst().orElse(null);
    }

    public Collection<Patient> getAllBlocked() {
        return patients.values().stream().filter(patient -> patient.isBlocked()).collect(Collectors.toCollection(ArrayList::new));
    }
}

package db.impl.postgres;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author Anna Guminskaya
 * @since 1/12/18
 */
public class DBConnection {

    public static Connection getDBConnection() {

        Connection dbConnection = null;

        try {

            Class.forName("org.postgresql.Driver");

        } catch (ClassNotFoundException e) {

            System.out.println(e.getMessage());

        }

        try {

            dbConnection = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/clinic",
                    "postgres", "bubu");
            return dbConnection;

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        }

        return dbConnection;

    }

    public static void closeConnection(PreparedStatement preparedStatement, Connection dbConnection ) {
        try {
            if (preparedStatement != null) {
                preparedStatement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

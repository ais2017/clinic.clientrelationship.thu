package db.impl.fake;

import db.DoctorInterface;
import logic.Doctor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.persistence.NoResultException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;


/**
 * Created by anna on 09.12.2017.
 */
public class DoctorRepository implements DoctorInterface {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public static AtomicLong id = new AtomicLong(0);

    public static Map<Long, Doctor> doctors = new HashMap<Long, Doctor>();

    public Doctor getOne(Long id) {
        if (doctors.get(id) == null) {
            throw new NoResultException();
        }
        return doctors.get(id);
    }

    public Collection<Doctor> getAll() {
        if (doctors.isEmpty()) {
            throw new NoResultException();
        }
        return doctors.values();
    }

    public Doctor save(Doctor doctor) {
        long key = id.incrementAndGet();
        doctor.setId(key);
        doctors.put(key, doctor);
        return doctor;
    }

    public void delete(Long id) {
        if (doctors.get(id) == null) {
            throw new NoResultException();
        }
        doctors.remove(id);
    }

    public void update(Long id, Doctor doctor) {
        if (doctors.get(id) == null) {
            throw new NoResultException();
        }
        doctors.remove(id);
        doctor.setId(id);
        doctors.put(id, doctor);
    }


    public Doctor generate() {
        return new Doctor(null,"Name", "Surname", "Patronymic", "Specialization");
    }

    public Doctor getBySurname(String surname) {
        return doctors.values().stream().filter(doctor -> doctor.getSurname().equals(surname)).findFirst().orElse(null);
    }

    public static void resetId() {
        id.set(0);
    }
}

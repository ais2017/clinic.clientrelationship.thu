package db.impl.fake;

import db.MedicalServiceInterface;
import logic.MedicalService;

import javax.persistence.NoResultException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by anna on 11.12.2017.
 */
public class MedicalServiceRepository implements MedicalServiceInterface {

    private static AtomicLong id = new AtomicLong(0);

    public static Map<Long, MedicalService> medicalServiceMap = new HashMap<Long, MedicalService>();

    public MedicalService getOne(Long id) {
        if (medicalServiceMap.get(id) == null) {
            throw new NoResultException();
        }
        return medicalServiceMap.get(id);
    }

    public Collection<MedicalService> getAll() {
        return medicalServiceMap.values();
    }

    public void delete(Long id) {
        if (medicalServiceMap.get(id) == null) {
            throw new NoResultException();
        }
        medicalServiceMap.remove(id);
    }

    public void update(Long id) {

    }

    public MedicalService generate() {
        MedicalService medicalService = new MedicalService(null, "Name", 100, "description");
        return medicalService;
    }

    @Override
    public MedicalService save(MedicalService medicalService) {
        long key = id.incrementAndGet();
        medicalService.setId(key);
        medicalServiceMap.put(key, medicalService);
        return medicalService;
    }

    public static void resetId() {
        id.set(0);
    }
}

package db.impl.fake;
import db.DiseaseInteface;
import logic.DiseaseHistory;

import javax.persistence.NoResultException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class DiseaseRepository implements DiseaseInteface {

    private static AtomicLong id = new AtomicLong(0);

    public static Map<Long, DiseaseHistory> diseaseHistoryMap = new HashMap<Long, DiseaseHistory>();

    @Override
    public DiseaseHistory getOne(Long id) {
        if (diseaseHistoryMap.get(id) == null) {
            throw new NoResultException();
        }
        return diseaseHistoryMap.get(id);
    }

    @Override
    public Collection<DiseaseHistory> getAll() {
        return diseaseHistoryMap.values();
    }

    @Override
    public void delete(Long id) {
        if (diseaseHistoryMap.get(id) == null) {
            throw new NoResultException();
        }
        diseaseHistoryMap.remove(id);
    }

    @Override
    public DiseaseHistory update(Long id, DiseaseHistory diseaseHistory) {
        if (diseaseHistoryMap.get(id) == null) {
            throw new NoResultException();
        }
        diseaseHistoryMap.remove(id);
        diseaseHistory.setId(id);
        diseaseHistoryMap.put(id, diseaseHistory);
        return diseaseHistory;
    }

    @Override
    public DiseaseHistory save(DiseaseHistory diseaseHistory) {
        long key = id.incrementAndGet();
        diseaseHistory.setId(key);
        diseaseHistoryMap.put(key, diseaseHistory);
        return diseaseHistory;
    }
}

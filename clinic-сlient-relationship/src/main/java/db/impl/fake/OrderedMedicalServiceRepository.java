package db.impl.fake;

import db.OrderedMedicalServiceInterface;
import logic.OrderedMedicalService;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * Created by anna on 14.12.2017.
 */
public class OrderedMedicalServiceRepository implements OrderedMedicalServiceInterface {
    
    private static AtomicLong id = new AtomicLong(0);

    public static Map<Long, OrderedMedicalService> orderedMedicalServiceMap = new HashMap<Long, OrderedMedicalService>();

    public OrderedMedicalService getOne(Long id) {
        if (orderedMedicalServiceMap.get(id) == null) {
            throw new NoResultException();
        }
        return orderedMedicalServiceMap.get(id);
    }

    public Collection<OrderedMedicalService> getAll() {
        return null;
    }

    public void delete(Long id) {
        if (orderedMedicalServiceMap.get(id) == null) {
            throw new NoResultException();
        }
        orderedMedicalServiceMap.remove(id);
    }
    public OrderedMedicalService update(Long id, OrderedMedicalService orderedMedicalService) {
        if (orderedMedicalServiceMap.get(id) == null) {
            throw new NoResultException();
        }
        orderedMedicalServiceMap.remove(id);
        orderedMedicalService.setId(id);
        orderedMedicalServiceMap.put(id, orderedMedicalService);
        return orderedMedicalService;
    }

    public OrderedMedicalService generate() {
        OrderedMedicalService orderedMedicalService = new OrderedMedicalService();
        return orderedMedicalService;
    }

    @Override
    public OrderedMedicalService save(OrderedMedicalService object) {
        long key = id.incrementAndGet();
        object.setId(key);
        orderedMedicalServiceMap.put(key, object);
        return object;
    }

    public static void resetId() {
        id.set(0);
    }

    @Override
    public Collection<OrderedMedicalService> getAllByPatientPolicyNumber(String policyNumber) {
        return orderedMedicalServiceMap.values().stream().filter(orderedMedicalService -> orderedMedicalService.getPatient().getPolicyNumber().equals(policyNumber)).collect(Collectors.toCollection(ArrayList::new));
    }
}

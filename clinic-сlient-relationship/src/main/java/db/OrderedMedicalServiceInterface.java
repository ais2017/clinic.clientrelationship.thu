package db;

import logic.OrderedMedicalService;
import logic.Patient;

import java.util.Collection;

public interface OrderedMedicalServiceInterface {
    OrderedMedicalService getOne(Long id);

    Collection<OrderedMedicalService> getAll();

    void delete(Long id);

    OrderedMedicalService update(Long id, OrderedMedicalService orderedMedicalService);

    OrderedMedicalService save(OrderedMedicalService object);

    Collection<OrderedMedicalService> getAllByPatientPolicyNumber(String policyNumber);

    OrderedMedicalService generate();
}

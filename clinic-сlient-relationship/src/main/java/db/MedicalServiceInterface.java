package db;

import logic.MedicalService;

import java.util.Collection;

public interface MedicalServiceInterface {
    MedicalService getOne(Long id);

    Collection<MedicalService> getAll();

    void delete(Long id);

    void update(Long id);

    MedicalService save(MedicalService medicalService);

    MedicalService generate();
}

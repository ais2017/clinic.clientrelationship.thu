package db;

import logic.Doctor;

import java.util.Collection;

public interface DoctorInterface {

    Doctor getOne(Long id);

    Collection<Doctor> getAll();

    Doctor save(Doctor doctor);

    public void delete(Long id);

    void update(Long id, Doctor doctor);

    Doctor getBySurname(String surname);

    Doctor generate();
}

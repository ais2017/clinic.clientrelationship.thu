import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by anna on 02.01.2018.
 */
@EnableScheduling
@PropertySource(value= {"classpath:application.properties"})
@EnableAutoConfiguration
public class Application {
    public static final String NAME = "NAME" ;
    public static final String SURNAME = "NAME" ;
    public static final String PATRONYMIC = "NAME" ;

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }
}

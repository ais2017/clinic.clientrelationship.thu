package scheduled;

import db.*;
import db.impl.fake.*;
import logic.OrderedMedicalService;
import logic.Patient;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

public class ScheduledJob {

    private DoctorInterface doctorDBInterface = new DoctorRepository();
    private PatientInteface patientDBInterface = new PatientRepository();
    private TimeTableInterface timeTableDBInterface = new TimeTableRepository();
    private AvailableAdmissionTimeInterface availableAdmissionTimeDBInterface = new AvailableAdmissionTimeRepository();
    private MedicalServiceInterface medicalServiceDBInterface = new MedicalServiceRepository();
    private OrderedMedicalServiceInterface orderedMedicalServiceDBInterface = new OrderedMedicalServiceRepository();
    private UserInteface userInteface = new UserRepository();

    @Scheduled(cron = "0 0 * * *")
    public void blockPatients() {
        Collection<Patient> patients = patientDBInterface.getAll();
        for (Patient patient : patients) {
            Collection<OrderedMedicalService> orderedMedicalServiceCollection = patient.getOrderedMedicalServices();
            if (orderedMedicalServiceCollection == null || orderedMedicalServiceCollection.size() == 0) {
                continue;
            }
            for (OrderedMedicalService orderedMedicalService : orderedMedicalServiceCollection) {
                if (!orderedMedicalService.isPaid()) {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(orderedMedicalService.getAdmissionDate());
                    int dayOfYear = cal.get(Calendar.DAY_OF_YEAR);
                    cal.setTime(new Date());
                    int dayOfYearNow = cal.get(Calendar.DAY_OF_YEAR);

                    if (dayOfYearNow > dayOfYear && (dayOfYearNow - dayOfYear) >= 21) {
                        Patient patient1 = new Patient(patient.getId(), patient.getPolicyNumber(), patient.getName(),
                                patient.getSurname(), patient.getPatronymic(), patient.getDiseaseHistories(), new ArrayList<>());
                        patient1.setBlocked(true);
                        patientDBInterface.update(patient1.getId(), patient1);
                    }
                }
            }
        }
    }
}


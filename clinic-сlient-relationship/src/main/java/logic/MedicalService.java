package logic;

import javax.persistence.Entity;

/**
 * Created by anna on 16.11.2017.
 */
public class MedicalService {

    private Long id;
    private String name;
    private Integer price;
    private String description;
    private Long doctorId;

    public MedicalService() {
    }

    public MedicalService(Long id, String name, Integer price, String description) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.description = description;
    }

    public MedicalService(Long id, String name, Integer price, String description, Long doctorId) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.description = description;
        this.doctorId = doctorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }

    @Override
    public String toString() {
        return "MedicalService{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                '}';
    }
}

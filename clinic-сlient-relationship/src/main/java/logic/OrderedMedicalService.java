package logic;

import javax.persistence.Entity;
import java.util.Date;

/**
 * Created by anna on 16.11.2017.
 */
public class OrderedMedicalService {

    private Long id;
    private MedicalService medicalService;
    private Date admissionDate;
    private Patient patient;
    private AvailableAdmissionTime admissionTime;
    private boolean isPaid;
    private Date payDate;
    private Long diseaseHistoryId;
    //Для маппинга на таблицы
    private Long patientId;
    private Long medicalServiceId;
    public OrderedMedicalService() {
    }

    public OrderedMedicalService(Long id, MedicalService medicalService, Date admissionDate, Patient patient, AvailableAdmissionTime admissionTime) {
        this.id = id;
        this.medicalService = medicalService;
        this.admissionDate = admissionDate;
        this.patient = patient;
        this.admissionTime = admissionTime;
    }

    public OrderedMedicalService(Long id, MedicalService medicalService, Date admissionDate, Patient patient, AvailableAdmissionTime admissionTime, boolean isPaid, Date payDate, Long diseaseHistoryId, Long patientId, Long medicalServiceId) {
        this.id = id;
        this.medicalService = medicalService;
        this.admissionDate = admissionDate;
        this.patient = patient;
        this.admissionTime = admissionTime;
        this.isPaid = isPaid;
        this.payDate = payDate;
        this.diseaseHistoryId = diseaseHistoryId;
        this.patientId = patientId;
        this.medicalServiceId = medicalServiceId;
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public Long getMedicalServiceId() {
        return medicalServiceId;
    }

    public void setMedicalServiceId(Long medicalServiceId) {
        this.medicalServiceId = medicalServiceId;
    }

    public Long getDiseaseHistoryId() {
        return diseaseHistoryId;
    }

    public void setDiseaseHistoryId(Long diseaseHistoryId) {
        this.diseaseHistoryId = diseaseHistoryId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MedicalService getMedicalService() {
        return medicalService;
    }

    public void setMedicalService(MedicalService medicalService) {
        this.medicalService = medicalService;
    }

    public Date getAdmissionDate() {
        return admissionDate;
    }

    public void setAdmissionDate(Date admissionDate) {
        this.admissionDate = admissionDate;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public AvailableAdmissionTime getAdmissionTime() {
        return admissionTime;
    }

    public void setAdmissionTime(AvailableAdmissionTime admissionTime) {
        this.admissionTime = admissionTime;
    }

    public boolean isPaid() {
        return isPaid;
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    @Override
    public String toString() {
        return "OrderedMedicalService{" +
                "id=" + id +
                ", medicalService=" + medicalService +
                ", admissionDate=" + admissionDate +
                ", patient=" + patient +
                ", admissionTime=" + admissionTime +
                ", isPaid=" + isPaid +
                ", payDate=" + payDate +
                '}';
    }
}
package logic;

import javax.persistence.Entity;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by anna on 16.11.2017.
 */
public class AvailableAdmissionTime {

    private Long id;
    private String startTime;
    private String endTime;
    private boolean isAvailable;
    private boolean isDoctorAbsent;
    private Long timeTableId;

    public AvailableAdmissionTime(String startTime, String endTime, boolean isAvailable, boolean isDoctorAbsent) {
        DateFormat sdf = new SimpleDateFormat("HH:mm");
        try {
            Date date1 = sdf.parse(startTime);
            Date date2 = sdf.parse(endTime);
            if (date2.before(date1)) {
                throw new IllegalArgumentException();
            }
        } catch (ParseException e) {
            throw new IllegalArgumentException();
        }
        this.startTime = startTime;
        this.endTime = endTime;
        this.isAvailable = isAvailable;
        this.isDoctorAbsent = isDoctorAbsent;
    }

    public AvailableAdmissionTime(Long id, String startTime, String endTime) {
        this.id = id;
        DateFormat sdf = new SimpleDateFormat("HH:mm");
        try {
            Date date1 = sdf.parse(startTime);
            Date date2 = sdf.parse(endTime);
            if (date2.before(date1)) {
                throw new IllegalArgumentException();
            }
        } catch (ParseException e) {
            throw new IllegalArgumentException();
        }
        this.startTime = startTime;
        this.endTime = endTime;
        this.isAvailable = true;
    }

    public Long getTimeTableId() {
        return timeTableId;
    }

    public void setTimeTableId(Long timeTableId) {
        this.timeTableId = timeTableId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        DateFormat sdf = new SimpleDateFormat("HH:mm");
        try {
            Date date1 = sdf.parse(startTime);
        } catch (ParseException e) {
            throw new IllegalArgumentException();
        }
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        DateFormat sdf = new SimpleDateFormat("HH:mm");
        try {
            Date date1 = sdf.parse(startTime);
            Date date2 = sdf.parse(endTime);
            if (date2.before(date1)) {
                throw new IllegalArgumentException();
            }
        } catch (ParseException e) {
            throw new IllegalArgumentException();
        }
        this.endTime = endTime;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public boolean isDoctorAbsent() {
        return isDoctorAbsent;
    }

    public void setDoctorAbsent(boolean doctorAbsent) {
        isDoctorAbsent = doctorAbsent;
    }

    @Override
    public String toString() {
        return "AvailableAdmissionTime{" +
                "id=" + id +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", isAvailable=" + isAvailable +
                ", isDoctorAbsent=" + isDoctorAbsent +
                '}';
    }
}

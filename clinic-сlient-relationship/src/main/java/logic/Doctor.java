package logic;

import authority.User;

import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.stream.Collectors;

/**
 * Created by anna on 16.11.2017.
 */
public class Doctor extends User {

    private Long id;
    private String name;
    private String surname;
    private String patronymic; //Отчество

    private String specialization;
    private ArrayList<TimeTable> timeTable;
    private ArrayList<MedicalService> services;

    public Doctor() {
    }

    public Doctor(Long id, String name, String surname, String patronymic, String specialization) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.specialization = specialization;
        this.role = Role.DOCTOR;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getSpecialization() {
        return specialization;
    }

    public ArrayList<TimeTable> getTimeTable() {
        return timeTable;
    }

    public void setTimeTable(ArrayList<TimeTable> timeTable) {
        this.timeTable = timeTable;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public ArrayList<MedicalService> getServices() {
        return services;
    }

    public void setServices(ArrayList<MedicalService> services) {
        this.services = services;
    }

    public ArrayList<AvailableAdmissionTime> getAvailableAdmissionTimeByTimeTable(TimeTable timeTable) {
        return timeTable.getAvailableAdmissionTimes().stream().filter(availableAdmissionTime -> availableAdmissionTime.isAvailable())
                        .collect(Collectors.toCollection(ArrayList::new));
    }

    public TimeTable getTimeTableByDate(Date enrollDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(enrollDate);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        for (TimeTable oneTimeTable : timeTable) {
            Date date1 = oneTimeTable.getDate();
            cal.setTime(date1);
            if (cal.get(Calendar.MONTH) == month && cal.get(Calendar.DAY_OF_MONTH) == day) {
                return oneTimeTable;
            }
        }
        return null;
    }


    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", specialization='" + specialization + '\'' +
                ", timeTable=" + timeTable +
                ", services=" + services +
                '}';
    }
}

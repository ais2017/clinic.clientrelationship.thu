package logic;

import authority.User;

import javax.persistence.Entity;
import java.util.ArrayList;

/**
 * Created by anna on 16.11.2017.
 */
public class Patient extends User {

    private Long id;
    private String policyNumber;
    private String name;
    private String surname;
    private String patronymic;

    private ArrayList<DiseaseHistory> diseaseHistories;
    private boolean isBlocked;
    private ArrayList<OrderedMedicalService> orderedMedicalServices;

    public Patient(Long id, String policyNumber, String name, String surname, String patronymic, ArrayList<DiseaseHistory> diseaseHistories, ArrayList<OrderedMedicalService> orderedMedicalServices) {
        this.id = id;
        this.policyNumber = policyNumber;
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.diseaseHistories = diseaseHistories;
        this.orderedMedicalServices = orderedMedicalServices;
        this.role = Role.USER;
    }


    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public ArrayList<DiseaseHistory> getDiseaseHistories() {
        return diseaseHistories;
    }

    public void setDiseaseHistories(ArrayList<DiseaseHistory> diseaseHistories) {
        this.diseaseHistories = diseaseHistories;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    public ArrayList<OrderedMedicalService> getOrderedMedicalServices() {
        return orderedMedicalServices;
    }

    public void setOrderedMedicalServices(ArrayList<OrderedMedicalService> orderedMedicalServices) {
        this.orderedMedicalServices = orderedMedicalServices;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "id=" + id +
                ", policyNumber='" + policyNumber + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", diseaseHistories=" + diseaseHistories +
                ", isBlocked=" + isBlocked;
    }
}

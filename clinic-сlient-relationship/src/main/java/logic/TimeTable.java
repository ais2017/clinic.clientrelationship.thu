package logic;

import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

/**
 * Created by anna on 16.11.2017.
 */
public class TimeTable {

    private Long id;
    private Date date;
    private ArrayList<AvailableAdmissionTime> availableAdmissionTimes;
    private Long doctorId;

    public TimeTable(Long id, Date date, ArrayList<AvailableAdmissionTime> availableAdmissionTimes) {
        this.id = id;
        this.date = date;
        this.availableAdmissionTimes = availableAdmissionTimes;
    }

    public TimeTable(Long id, Date date, ArrayList<AvailableAdmissionTime> availableAdmissionTimes, Long doctorId) {
        this.id = id;
        this.date = date;
        this.availableAdmissionTimes = availableAdmissionTimes;
        this.doctorId = doctorId;
    }

    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ArrayList<AvailableAdmissionTime> getAvailableAdmissionTimes() {
        return availableAdmissionTimes;
    }

    public void setAvailableAdmissionTimes(ArrayList<AvailableAdmissionTime> availableAdmissionTimes) {
        this.availableAdmissionTimes = availableAdmissionTimes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "TimeTable{" +
                "id=" + id +
                ", date=" + date +
                ", availableAdmissionTimes=" + availableAdmissionTimes +
                '}';
    }
}

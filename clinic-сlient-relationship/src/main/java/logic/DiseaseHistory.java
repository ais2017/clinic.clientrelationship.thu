package logic;

import logic.OrderedMedicalService;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by anna on 16.11.2017.
 */
public class DiseaseHistory {

    private Long id;
    private Date admissionDate;
    private String description;
    private Long patientId;
    private ArrayList<OrderedMedicalService> orderedMedicalServices;

    public DiseaseHistory(Long id, Date admissionDate, String description) {
        this.id = id;
        this.admissionDate = admissionDate;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getAdmissionDate() {
        return admissionDate;
    }

    public void setAdmissionDate(Date admissionDate) {
        this.admissionDate = admissionDate;
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public ArrayList<OrderedMedicalService> getOrderedMedicalServices() {
        return orderedMedicalServices;
    }

    public void setOrderedMedicalServices(ArrayList<OrderedMedicalService> orderedMedicalServices) {
        this.orderedMedicalServices = orderedMedicalServices;
    }

    @Override
    public String toString() {
        return "DiseaseHistory{" +
                "id=" + id +
                ", admissionDate=" + admissionDate +
                ", description='" + description + '\'' +
                ", orderedMedicalServices=" + orderedMedicalServices +
                '}';
    }
}

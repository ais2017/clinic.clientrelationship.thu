package authority;

import javax.persistence.Entity;
import java.io.Serializable;

public class User implements Serializable {

    public enum Role {
        USER,
        DOCTOR,
        ADMIN
    }

    private Long id;

    protected Role role;

    protected String login;

    protected String pasword;

    private Long doctorId;

    public User() {
    }

    public User(String login, String pasword) {
        this.login = login;
        this.pasword = pasword;
    }

    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasword() {
        return pasword;
    }

    public void setPasword(String pasword) {
        this.pasword = pasword;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

package UIs;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;

@Theme("valo")
@SuppressWarnings("serial")
public class RootUI extends UI {

    @WebServlet(value = {"/*", "/VAADIN/*"}, asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = RootUI.class)
    public static class Servlet extends VaadinServlet {
    }

    @Override
    protected void init(VaadinRequest request) {
        VerticalLayout layout = new VerticalLayout();
        layout.setMargin(true);
        layout.setSpacing(true);
        setContent(layout);
        Notification.show(request.getWrappedSession().getId(),
                Notification.Type.HUMANIZED_MESSAGE);

        layout.addComponent(new Label("Авторизация в системе"));
        layout.addComponent(new Label("Выберите свою роль: "));

        layout.addComponent(new Button("Пациент", event -> {// Java 8
            // Redirect this page immediately
            layout.removeAllComponents();
            getPage().setLocation("/patient/login");
        }));

        layout.addComponent(new Button("Сотрудник", event -> {// Java 8
            // Redirect this page immediately
            layout.removeAllComponents();
            getPage().setLocation("/doctor/login");
        }));

    }

}
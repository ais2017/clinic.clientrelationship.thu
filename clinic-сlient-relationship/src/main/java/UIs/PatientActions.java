package UIs;

import javax.servlet.annotation.WebServlet;

import authority.User;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;

import java.nio.file.AccessDeniedException;

@Theme("valo")
@SuppressWarnings("serial")
public class PatientActions extends UI {

    @WebServlet(value = {"/patient/actions/*", "/VAADIN5/*"}, asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = PatientActions.class)
    public static class Servlet extends VaadinServlet {
    }

    @Override
    protected void init(VaadinRequest request) {
        final VerticalLayout layout = new VerticalLayout();
        layout.setMargin(true);
        layout.setSpacing(true);
        setContent(layout);

        layout.addComponent(new Label("Действия залогиненного пользователя: "));
        layout.addComponent(new Button("Зарегистрироваться на приём", event -> {
                getPage().setLocation("/patient/enroll");
        }));
        layout.addComponent(new Button("Отменить запись", event -> {
            getPage().setLocation("/patient/deenroll");
        }));
        layout.addComponent(new Button("Оплата услуг", event -> {
            getPage().setLocation("/patient/payForService");
        }));
        layout.addComponent(new Button("Показать историю болезни", event -> {
            getPage().setLocation("/patient/diseaseHistory");
        }));

    }

}
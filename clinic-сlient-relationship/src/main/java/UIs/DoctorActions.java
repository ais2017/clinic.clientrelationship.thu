package UIs;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;

@Theme("valo")
@SuppressWarnings("serial")
public class DoctorActions extends UI {

    @WebServlet(value = {"/doctor/actions/*", "/VAADIN4/*"}, asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = DoctorActions.class)
    public static class Servlet extends VaadinServlet {
    }

    @Override
    protected void init(VaadinRequest request) {
        final VerticalLayout layout = new VerticalLayout();
        layout.setMargin(true);
        layout.setSpacing(true);
        setContent(layout);

        layout.addComponent(new Label("Действия залогиненного доктора: "));
    }

}
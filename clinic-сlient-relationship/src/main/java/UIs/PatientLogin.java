package UIs;

import javax.servlet.annotation.WebServlet;

import authority.User;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import db.*;
import db.impl.fake.*;
import logic.Patient;

import java.nio.file.AccessDeniedException;
import java.util.ArrayList;

@Theme("valo")
@SuppressWarnings("serial")
public class PatientLogin extends UI {

    private DoctorInterface doctorDBInterface = new DoctorRepository();
    private PatientInteface patientDBInterface = new PatientRepository();
    private TimeTableInterface timeTableDBInterface = new TimeTableRepository();
    private AvailableAdmissionTimeInterface availableAdmissionTimeDBInterface = new AvailableAdmissionTimeRepository();
    private MedicalServiceInterface medicalServiceDBInterface = new MedicalServiceRepository();
    private OrderedMedicalServiceInterface orderedMedicalServiceDBInterface = new OrderedMedicalServiceRepository();
    private UserInteface userInteface = new UserRepository();
    private DiseaseInteface diseaseInteface = new DiseaseRepository();


    @WebServlet(value = {"/patient/login/*", "/VAADIN1/*"}, asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = PatientLogin.class)
    public static class Servlet extends VaadinServlet {
    }

    @Override
    protected void init(VaadinRequest request) {
        final VerticalLayout layout = new VerticalLayout();
        layout.setMargin(true);
        layout.setSpacing(true);
        setContent(layout);
        layout.addComponent(new Label("Авторизация в системе: "));
        TextField tf = new TextField("Номер полиса: ");
        layout.addComponent(tf);
        Patient patient = new Patient(null, "123", "Name", "Surname", "Patronymic", new ArrayList<>(), new ArrayList<>());
        patient = patientDBInterface.save(patient);
        Notification.show(request.getWrappedSession().getId(),
                Notification.Type.HUMANIZED_MESSAGE);
        Scenario scenario = new Scenario(doctorDBInterface, patientDBInterface, timeTableDBInterface, availableAdmissionTimeDBInterface,
                medicalServiceDBInterface, orderedMedicalServiceDBInterface, userInteface, diseaseInteface);
        layout.addComponent(new Button("Ввод", event -> {
                try {
                    scenario.authPatient(tf.getValue());
                    Notification.show("Авторизация успешна");
                    User user = new User();
                    user.setRole(User.Role.USER);
                    SharedConstants.THREAD_POOL_LOGIN.put(request.getWrappedSession().getId(), user);
                    getPage().setLocation("/patient/actions");
                } catch (AccessDeniedException e) {
                    Notification.show("Авторизация неуспешна",
                            "Полис не найден или пациент заблокирован",
                            Notification.Type.HUMANIZED_MESSAGE);
                    SharedConstants.THREAD_POOL_LOGIN.remove(request.getWrappedSession().getId());
                }

        }));
        layout.addComponent(new Button("На главную", event -> {// Java 8
            getPage().setLocation("/");
        }));

    }

}
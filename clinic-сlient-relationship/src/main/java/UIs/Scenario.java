package UIs;

import authority.User;
import db.*;
import db.impl.fake.*;
import logic.*;
import scheduled.ScheduledJob;

import javax.persistence.NoResultException;
import java.nio.file.AccessDeniedException;
import java.util.*;
import java.util.stream.Collectors;


/**
 * Created by anna on 08.12.2017.
 */
public class Scenario {

    private DoctorInterface doctorDBInterface = new DoctorRepository();
    private PatientInteface patientDBInterface = new PatientRepository();
    private TimeTableInterface timeTableDBInterface = new TimeTableRepository();
    private AvailableAdmissionTimeInterface availableAdmissionTimeDBInterface = new AvailableAdmissionTimeRepository();
    private MedicalServiceInterface medicalServiceDBInterface = new MedicalServiceRepository();
    private OrderedMedicalServiceInterface orderedMedicalServiceDBInterface = new OrderedMedicalServiceRepository();
    private UserInteface userInteface = new UserRepository();
    private DiseaseInteface diseaseInteface = new DiseaseRepository();

    public Scenario(DoctorInterface doctorDBInterface, PatientInteface patientDBInterface, TimeTableInterface timeTableDBInterface, AvailableAdmissionTimeInterface availableAdmissionTimeDBInterface, MedicalServiceInterface medicalServiceDBInterface, OrderedMedicalServiceInterface orderedMedicalServiceDBInterface, UserInteface userInteface, DiseaseInteface diseaseInteface) {
        this.doctorDBInterface = doctorDBInterface;
        this.patientDBInterface = patientDBInterface;
        this.timeTableDBInterface = timeTableDBInterface;
        this.availableAdmissionTimeDBInterface = availableAdmissionTimeDBInterface;
        this.medicalServiceDBInterface = medicalServiceDBInterface;
        this.orderedMedicalServiceDBInterface = orderedMedicalServiceDBInterface;
        this.userInteface = userInteface;
        this.diseaseInteface = diseaseInteface;
    }

    public boolean authPatient(String policyNumber) throws AccessDeniedException {
        Patient patient = patientDBInterface.getByPolicyNumber(policyNumber);
        if (patient == null) {
            throw new AccessDeniedException("");
        }
        if (patient.isBlocked()) {
            throw new AccessDeniedException("");
        }
        return true;
    }

    public User authStaff(String login, String password) throws AccessDeniedException {
        User user = userInteface.getByLogin(login);
        if (user == null) {
            throw new AccessDeniedException("");
        }

        if (!user.getPasword().equals(password)) {
            throw new AccessDeniedException("");
        }
        return user;
    }

    public ArrayList<AvailableAdmissionTime> showAvailableAdmissionTime(String surname) throws NoResultException {

        Doctor doctor = doctorDBInterface.getBySurname(surname);
        if (doctor == null) {
            throw new NoResultException();
        }
        ArrayList<AvailableAdmissionTime> availableAdmissionTimes = new ArrayList<>();
        ArrayList<TimeTable> timeTables = doctor.getTimeTable();
        if (timeTables == null || timeTables.size() == 0) {
            throw new NoResultException();
        }
        for (TimeTable timeTable1 : timeTables) {
            List<AvailableAdmissionTime> availableAdmissionTimes1 = timeTable1.getAvailableAdmissionTimes().stream().filter(x -> x.isAvailable()).collect(Collectors.toList());
            availableAdmissionTimes.addAll(availableAdmissionTimes1);
        }
        return availableAdmissionTimes;
    }

    public void enroll(String surname, Date date, String startTime, String endTime, MedicalService medicalService, Patient patient) {
        Doctor doctor = doctorDBInterface.getBySurname(surname);
        if (doctor == null) {
            throw new NoResultException();
        }
        TimeTable timeTable = doctor.getTimeTableByDate(date);
        ArrayList<AvailableAdmissionTime> availableAdmissionTimes = doctor.getAvailableAdmissionTimeByTimeTable(timeTable);
        AvailableAdmissionTime availableAdmissionTime  = availableAdmissionTimes.stream().filter
                (x -> x.getStartTime().equals(startTime) && x.getEndTime().equals(endTime)).findFirst().orElse(null);

        if (availableAdmissionTime == null) {
            throw new NoResultException();
        }

        OrderedMedicalService orderedMedicalService = new OrderedMedicalService(null, medicalService, date, patient, availableAdmissionTime);
        orderedMedicalService = orderedMedicalServiceDBInterface.save(orderedMedicalService);
        patient.getOrderedMedicalServices().add(orderedMedicalService);
        Doctor newDoctor = new Doctor(doctor.getId(), doctor.getName(), doctor.getSurname(), doctor.getPatronymic(), doctor.getSpecialization());
        availableAdmissionTimes.remove(availableAdmissionTime);
        availableAdmissionTime.setAvailable(false);
        availableAdmissionTimes.add(availableAdmissionTime);
        TimeTable newDoctorTimeTable = new TimeTable(timeTable.getId(), timeTable.getDate(), availableAdmissionTimes);
        newDoctorTimeTable = timeTableDBInterface.update(timeTable.getId(), newDoctorTimeTable);
        ArrayList<TimeTable> timeTables = doctor.getTimeTable();
        timeTables.remove(timeTable);
        timeTables.add(newDoctorTimeTable);
        newDoctor.setTimeTable(timeTables);

        doctorDBInterface.update(newDoctor.getId(), newDoctor);
    }

    public void deEnroll(String surname, OrderedMedicalService orderedMedicalService, Patient patient) {
        Doctor doctor = doctorDBInterface.getBySurname(surname);
        if (doctor == null) {
            throw new NoResultException();
        }

        TimeTable timeTable = doctor.getTimeTableByDate(orderedMedicalService.getAdmissionDate());
        if (timeTable == null) {
            throw new NoResultException();
        }

        OrderedMedicalService orderedMedicalService1 = orderedMedicalServiceDBInterface.getOne(orderedMedicalService.getId());
        if (orderedMedicalService1 == null) {
            throw new NoResultException();
        }
        ArrayList<AvailableAdmissionTime> availableAdmissionTimes = timeTable.getAvailableAdmissionTimes();
        orderedMedicalServiceDBInterface.delete(orderedMedicalService.getId());

        Doctor newDoctor = new Doctor(doctor.getId(), doctor.getName(), doctor.getSurname(), doctor.getPatronymic(), doctor.getSpecialization());
        AvailableAdmissionTime availableAdmissionTime = orderedMedicalService1.getAdmissionTime();
        availableAdmissionTimes.remove(orderedMedicalService1.getAdmissionTime());
        availableAdmissionTime.setAvailable(true);
        availableAdmissionTimes.add(availableAdmissionTime);
        TimeTable newDoctorTimeTable = new TimeTable(timeTable.getId(), timeTable.getDate(), availableAdmissionTimes);
        newDoctorTimeTable = timeTableDBInterface.update(timeTable.getId(), newDoctorTimeTable);
        ArrayList<TimeTable> timeTables = doctor.getTimeTable();
        timeTables.remove(timeTable);
        timeTables.add(newDoctorTimeTable);
        newDoctor.setTimeTable(timeTables);

        doctorDBInterface.update(newDoctor.getId(), newDoctor);
    }

    public TimeTable editTimeTable(String surname, Date date, ArrayList<AvailableAdmissionTime> availableAdmissionTimes) {
        Doctor doctor = doctorDBInterface.getBySurname(surname);
        if (doctor == null) {
            throw new NoResultException();
        }
        ArrayList<AvailableAdmissionTime> availableAdmissionTimesNew = new ArrayList<>();
        TimeTable timeTable = doctor.getTimeTableByDate(date);
        if (timeTable == null) {
            for (AvailableAdmissionTime availableAdmissionTime: availableAdmissionTimes) {
                availableAdmissionTimesNew.add(availableAdmissionTimeDBInterface.save(availableAdmissionTime));
            }
            timeTable = new TimeTable(null, date, availableAdmissionTimesNew);
            return timeTableDBInterface.save(timeTable);
        }
        ArrayList<AvailableAdmissionTime> availableAdmissionTimesCurrent = doctor.getAvailableAdmissionTimeByTimeTable(timeTable);
        if (availableAdmissionTimesCurrent == null || availableAdmissionTimes.size() == 0) {
            for (AvailableAdmissionTime availableAdmissionTime: availableAdmissionTimes) {
                availableAdmissionTimesNew.add(availableAdmissionTimeDBInterface.save(availableAdmissionTime));
            }
            timeTable.setAvailableAdmissionTimes(availableAdmissionTimesNew);
            return timeTableDBInterface.update(timeTable.getId(), timeTable);
        }
        for (AvailableAdmissionTime availableAdmissionTime: availableAdmissionTimesCurrent) {
            availableAdmissionTimeDBInterface.delete(availableAdmissionTime.getId());
        }
        for (AvailableAdmissionTime availableAdmissionTime: availableAdmissionTimes) {
            availableAdmissionTimesNew.add(availableAdmissionTimeDBInterface.save(availableAdmissionTime));
        }
        timeTable.setAvailableAdmissionTimes(availableAdmissionTimesNew);
        return timeTableDBInterface.update(timeTable.getId(), timeTable);
    }

    public void blockPatient() {
        ScheduledJob scheduledJob = new ScheduledJob();
        scheduledJob.blockPatients();
    }

    public Collection<Patient> getUnreliablePateints() {
        Collection<Patient> patients = patientDBInterface.getAll();
        Collection<Patient> result = new ArrayList<>();
        for (Patient patient : patients) {
            Collection<OrderedMedicalService> orderedMedicalServiceCollection = patient.getOrderedMedicalServices();
            if (orderedMedicalServiceCollection == null || orderedMedicalServiceCollection.size() == 0) {
                continue;
            }
            for (OrderedMedicalService orderedMedicalService : orderedMedicalServiceCollection) {
                if (!orderedMedicalService.isPaid()) {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(orderedMedicalService.getAdmissionDate());
                    int dayOfYear = cal.get(Calendar.DAY_OF_YEAR);
                    cal.setTime(new Date());
                    int dayOfYearNow = cal.get(Calendar.DAY_OF_YEAR);
                    if (dayOfYearNow > dayOfYear && (dayOfYearNow - dayOfYear) >= 14) {
                        result.add(patient);
                    }
                }
            }
        }
        return result;
    }

    public void addMedicalHistory(String policyNumber, String description, OrderedMedicalService orderedMedicalService) {
        Patient patient = patientDBInterface.getByPolicyNumber(policyNumber);
        if (patient == null) {
            throw new NoResultException();
        }
        ArrayList<DiseaseHistory> diseaseHistories = patient.getDiseaseHistories();
        if (diseaseHistories == null) {
            diseaseHistories = new ArrayList<>();
        }
        DiseaseHistory diseaseHistory = new DiseaseHistory(null, orderedMedicalService.getAdmissionDate(), description);
        diseaseHistory = diseaseInteface.save(diseaseHistory);
        diseaseHistories.add(diseaseHistory);

        Patient patient1 = new Patient(patient.getId(), patient.getPolicyNumber(), patient.getName(), patient.getSurname(), patient.getPatronymic(),
                diseaseHistories, patient.getOrderedMedicalServices());
        patientDBInterface.update(patient.getId(), patient1);
    }

    private Collection<OrderedMedicalService> showUnPayed(String policyNumber) {
        Patient patient = patientDBInterface.getByPolicyNumber(policyNumber);
        return patient.getOrderedMedicalServices().stream().filter(orderedMedicalService -> !orderedMedicalService.isPaid()).collect(Collectors.toCollection(ArrayList::new));
    }

    public Integer getTotalPrice(Collection<OrderedMedicalService> orderedMedicalServices) {
        Integer totalPrice = 0;
        for (OrderedMedicalService orderedMedicalService: orderedMedicalServices) {
            totalPrice += orderedMedicalService.getMedicalService().getPrice();
        }
        return totalPrice;
    }

    public Collection<OrderedMedicalService> payForService(String policyNumber, Integer totalPrice) {
        Collection<OrderedMedicalService> orderedMedicalServices = showUnPayed(policyNumber);
        if (!totalPrice.equals(getTotalPrice(orderedMedicalServices))) {
            throw new IllegalArgumentException();
        }
        Collection<OrderedMedicalService> result = new ArrayList<>();
        for (OrderedMedicalService orderedMedicalService1: orderedMedicalServices) {
            OrderedMedicalService updatedOrderedMedicalService = new OrderedMedicalService(orderedMedicalService1.getId(),
                    orderedMedicalService1.getMedicalService(), orderedMedicalService1.getAdmissionDate(),
                    orderedMedicalService1.getPatient(), orderedMedicalService1.getAdmissionTime());
            updatedOrderedMedicalService.setPaid(true);
            result.add(orderedMedicalServiceDBInterface.update(updatedOrderedMedicalService.getId(), updatedOrderedMedicalService));
        }
        return result;
    }
}

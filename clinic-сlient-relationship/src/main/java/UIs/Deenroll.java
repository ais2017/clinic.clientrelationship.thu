package UIs;

import javax.servlet.annotation.WebServlet;

import authority.User;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;

import java.nio.file.AccessDeniedException;

@Theme("valo")
@SuppressWarnings("serial")
public class Deenroll extends UI {

    @WebServlet(value = {"/patient/deenroll/*", "/VAADIN7/*"}, asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = Deenroll.class)
    public static class Servlet extends VaadinServlet {
    }

    @Override
    protected void init(VaadinRequest request) {
        final VerticalLayout layout = new VerticalLayout();
        layout.setMargin(true);
        layout.setSpacing(true);
        setContent(layout);

        layout.addComponent(new Label("Deenroll "));

    }

}
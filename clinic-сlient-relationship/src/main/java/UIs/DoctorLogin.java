package UIs;

import javax.servlet.annotation.WebServlet;

import authority.User;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import db.*;
import db.impl.fake.*;

import java.nio.file.AccessDeniedException;

@Theme("valo")
@SuppressWarnings("serial")
public class DoctorLogin extends UI {

    private DoctorInterface doctorDBInterface = new DoctorRepository();
    private PatientInteface patientDBInterface = new PatientRepository();
    private TimeTableInterface timeTableDBInterface = new TimeTableRepository();
    private AvailableAdmissionTimeInterface availableAdmissionTimeDBInterface = new AvailableAdmissionTimeRepository();
    private MedicalServiceInterface medicalServiceDBInterface = new MedicalServiceRepository();
    private OrderedMedicalServiceInterface orderedMedicalServiceDBInterface = new OrderedMedicalServiceRepository();
    private UserInteface userInteface = new UserRepository();
    private DiseaseInteface diseaseInteface = new DiseaseRepository();

    @WebServlet(value = {"/doctor/login/*", "/VAADIN2/*"}, asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = DoctorLogin.class)
    public static class Servlet extends VaadinServlet {
    }

    @Override
    protected void init(VaadinRequest request) {
        final VerticalLayout layout = new VerticalLayout();
        layout.setMargin(true);
        layout.setSpacing(true);
        setContent(layout);
        layout.addComponent(new Label("Авторизация в системе: "));
        TextField inputLogin = new TextField("Логин: ");
        layout.addComponent(inputLogin);
        TextField inputPassword = new TextField("Пароль: ");
        layout.addComponent(inputPassword);
        Button button = new Button("Ввод");
        layout.addComponent(button);
        String login = "DOCTOR";
        String password = "PASSWORD";
        User user = new User(login, password);
        user.setRole(User.Role.DOCTOR);
        userInteface.save(user);
        User user2 = new User("ADMIN", password);
        user2.setRole(User.Role.ADMIN);
        userInteface.save(user2);
        Notification.show(request.getWrappedSession().getId(),
                Notification.Type.HUMANIZED_MESSAGE);
        Scenario scenario = new Scenario(doctorDBInterface, patientDBInterface, timeTableDBInterface, availableAdmissionTimeDBInterface,
                medicalServiceDBInterface, orderedMedicalServiceDBInterface, userInteface, diseaseInteface);
        button.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    User user = scenario.authStaff(inputLogin.getValue(), inputPassword.getValue());
                    Notification.show("Авторизация успешна");
                    SharedConstants.THREAD_POOL_LOGIN.put(request.getWrappedSession().getId(), user);
                    if (user.getRole() == User.Role.DOCTOR) {
                        getPage().setLocation("/doctor/actions");
                    }
                    if (user.getRole() == User.Role.ADMIN) {
                        getPage().setLocation("/admin/actions");
                    }
                } catch (AccessDeniedException e) {
                    Notification.show("Авторизация неуспешна",
                            "Неправильный логин/пароль",
                            Notification.Type.HUMANIZED_MESSAGE);
                    SharedConstants.THREAD_POOL_LOGIN.remove(request.getWrappedSession().getId());
                }

            }
        });
        layout.addComponent(new Button("На главную", event -> {// Java 8
            getPage().setLocation("/");
        }));

    }

}
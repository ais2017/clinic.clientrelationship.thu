package UIs;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.Container;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import db.*;
import db.impl.fake.*;
import logic.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Theme("valo")
@SuppressWarnings("serial")
public class Enroll extends UI {

    private DoctorInterface doctorDBInterface = new DoctorRepository();
    private PatientInteface patientDBInterface = new PatientRepository();
    private TimeTableInterface timeTableDBInterface = new TimeTableRepository();
    private AvailableAdmissionTimeInterface availableAdmissionTimeDBInterface = new AvailableAdmissionTimeRepository();
    private MedicalServiceInterface medicalServiceDBInterface = new MedicalServiceRepository();
    private OrderedMedicalServiceInterface orderedMedicalServiceDBInterface = new OrderedMedicalServiceRepository();
    private UserInteface userInteface = new UserRepository();
    private DiseaseInteface diseaseInteface = new DiseaseRepository();

    @WebServlet(value = {"/patient/enroll/*", "/VAADIN6/*"}, asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = Enroll.class)
    public static class Servlet extends VaadinServlet {
    }

    @Override
    protected void init(VaadinRequest request) {
        final VerticalLayout layout = new VerticalLayout();
        layout.setMargin(true);
        layout.setSpacing(true);
        setContent(layout);

        /**/
        AvailableAdmissionTime availableAdmissionTime = availableAdmissionTimeDBInterface.generate("11:00", "12:00");
        TimeTable timeTable = timeTableDBInterface.generate(new Date());
        ArrayList<AvailableAdmissionTime> availableAdmissionTimes = new ArrayList<>();
        availableAdmissionTimes.add(availableAdmissionTime);
        timeTable.setAvailableAdmissionTimes(availableAdmissionTimes);
        Doctor doctor = doctorDBInterface.generate();
        ArrayList<TimeTable> timeTables = new ArrayList<>();
        timeTable = timeTableDBInterface.save(timeTable);
        timeTables.add(timeTable);
        doctor.setTimeTable(timeTables);
        doctorDBInterface.save(doctor);
        Doctor doctor2 = doctorDBInterface.generate();
        doctor2.setSurname("Торопов");
        doctorDBInterface.save(doctor2);
        MedicalService medicalService = new MedicalService(null, "Name", 100, "Description");
        medicalServiceDBInterface.save(medicalService);
        Patient patient = new Patient(null, "1", "Name", "Surname", "Patronymic", new ArrayList<>(), new ArrayList<>());
        final Patient resPatient = patientDBInterface.save(patient);
        /**/
        Scenario scenario = new Scenario(doctorDBInterface, patientDBInterface, timeTableDBInterface, availableAdmissionTimeDBInterface,
                medicalServiceDBInterface, orderedMedicalServiceDBInterface, userInteface, diseaseInteface);
        TextField startTimeField = new TextField("Время начала записи");
        TextField endTimeField = new TextField("Окончание записи");
        DateField dateField =  new DateField("Дата приема");
        Collection<Doctor> doctors = doctorDBInterface.getAll();
        Collection<String> res = new ArrayList<>();
        for (Doctor doctor1: doctors) {
            res.add(doctor1.getSurname());
        }
        ListSelect listSelect = new ListSelect("Фамилия врача ") ;
        listSelect.setRows(res.size());
        listSelect.addItems(res);
        layout.addComponent(listSelect);
        layout.addComponent(dateField);
        layout.addComponent(startTimeField);
        layout.addComponent(endTimeField);
        ListSelect listSelect1 = new ListSelect("Выберите время приема");

        layout.addComponent(new Button("Ввод", event -> {
            try {
                Notification.show(listSelect.getValue().toString());
                scenario.enroll(listSelect.getValue().toString(), dateField.getValue(), startTimeField.getValue(), endTimeField.getValue(), medicalService, resPatient);
                Notification.show("Cценарий выполнен");
            } catch (Exception e) {
                Notification.show("Иксэпшн поймали",
                        Notification.Type.HUMANIZED_MESSAGE);
                e.printStackTrace();
            }

        }));
        layout.addComponent(new Button("Назад", event -> {// Java 8
            getPage().setLocation("/patient/actions");
        }));
    }

}
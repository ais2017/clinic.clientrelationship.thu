package UIs;

import javax.servlet.annotation.WebServlet;

import authority.User;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;

import java.nio.file.AccessDeniedException;

@Theme("valo")
@SuppressWarnings("serial")
public class PayForService extends UI {

    @WebServlet(value = {"/patient/payForService/*", "/VAADIN8/*"}, asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = PayForService.class)
    public static class Servlet extends VaadinServlet {
    }

    @Override
    protected void init(VaadinRequest request) {
        final VerticalLayout layout = new VerticalLayout();
        layout.setMargin(true);
        layout.setSpacing(true);
        setContent(layout);

        layout.addComponent(new Label("PayForService "));

    }

}
CREATE TYPE roletype AS ENUM ('User', 'Doctor', 'Admin');

CREATE TABLE UserEntity (
  id BIGSERIAL,
  login VARCHAR(255),
  password VARCHAR(255),
  role roleType,
  doctorId BIGINT,
  PRIMARY KEY (id)
);

CREATE TABLE Patient (
  id BIGSERIAL,
  policyNumber VARCHAR(255),
  name VARCHAR(255),
  surname VARCHAR (255),
  patronymic VARCHAR (255),
  PRIMARY KEY (id),
  CONSTRAINT UQ_Patient_policyNumber UNIQUE (policyNumber)

);

CREATE TABLE Doctor (
  id BIGSERIAL,
  name VARCHAR(255),
  surname VARCHAR (255),
  patronymic VARCHAR (255),
  specialization VARCHAR(255),
  PRIMARY KEY (id),
  CONSTRAINT UQ_Doctor_Surname UNIQUE (surname)
);

CREATE TABLE MedicalService (
  id BIGSERIAL,
  name VARCHAR(255),
  price INTEGER,
  description VARCHAR(255),
  doctorId BIGINT REFERENCES Doctor(id),
  PRIMARY KEY (id)
);

CREATE TABLE DiseaseHistory (
  id BIGSERIAL,
  admissionDate TIMESTAMP,
  description VARCHAR(255),
  patientId BIGINT REFERENCES Patient,
  PRIMARY KEY (id)
);

CREATE TABLE OrderedMedicalService (
  id BIGSERIAL,
  admissionDate TIMESTAMP,
  patientId BIGINT REFERENCES Patient(id),
  isPaid BOOLEAN DEFAULT FALSE,
  payDate TIMESTAMP,
  diseaseHistoryId BIGINT REFERENCES DiseaseHistory,
  medicalServiceId BIGINT REFERENCES MedicalService,
  PRIMARY KEY (id)
);

CREATE TABLE TimeTable (
  id BIGSERIAL,
  date TIMESTAMP,
  doctorId BIGINT REFERENCES Doctor(id),
  PRIMARY KEY (id)
);

CREATE TABLE AvailableAdmissionTime (
  id BIGSERIAL,
  startTime VARCHAR(5),
  endTime VARCHAR(5),
  isAvailable BOOLEAN DEFAULT TRUE,
  isDoctorAbsent BOOLEAN DEFAULT FALSE,
  timeTableId BIGINT REFERENCES TimeTable,
  PRIMARY KEY (id)
);

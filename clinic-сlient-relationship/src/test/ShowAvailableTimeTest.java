import db.*;
import logic.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import db.impl.fake.*;
import UIs.Scenario;
import javax.persistence.NoResultException;
import javax.print.Doc;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

/**
 * Created by anna on 16.11.2017.
 */
public class ShowAvailableTimeTest {

    private DoctorInterface doctorDBInterface = new DoctorRepository();
    private PatientInteface patientDBInterface = new PatientRepository();
    private TimeTableInterface timeTableDBInterface = new TimeTableRepository();
    private AvailableAdmissionTimeInterface availableAdmissionTimeDBInterface = new AvailableAdmissionTimeRepository();
    private MedicalServiceInterface medicalServiceDBInterface = new MedicalServiceRepository();
    private OrderedMedicalServiceInterface orderedMedicalServiceDBInterface = new OrderedMedicalServiceRepository();
    private UserInteface userInteface = new UserRepository();
    private DiseaseInteface diseaseInteface = new DiseaseRepository();

    @Test(expected = NoResultException.class)
    public void showAvailableAdmissionTimeNoResult() {
        Scenario scenario = new Scenario(doctorDBInterface, patientDBInterface, timeTableDBInterface, availableAdmissionTimeDBInterface,
                medicalServiceDBInterface, orderedMedicalServiceDBInterface, userInteface, diseaseInteface);
        scenario.showAvailableAdmissionTime("Surname");
    }

    @Test
    public void showAvailableAdmissionTime() {
        Scenario scenario = new Scenario(doctorDBInterface, patientDBInterface, timeTableDBInterface, availableAdmissionTimeDBInterface,
                medicalServiceDBInterface, orderedMedicalServiceDBInterface, userInteface, diseaseInteface);
        AvailableAdmissionTime availableAdmissionTime = availableAdmissionTimeDBInterface.generate("11:00", "12:00");
        TimeTable timeTable = timeTableDBInterface.generate(new Date());
        ArrayList<AvailableAdmissionTime> availableAdmissionTimes = new ArrayList<>();
        availableAdmissionTimes.add(availableAdmissionTime);
        timeTable.setAvailableAdmissionTimes(availableAdmissionTimes);
        Doctor doctor = doctorDBInterface.generate();
        ArrayList<TimeTable> timeTables = new ArrayList<>();
        timeTables.add(timeTable);
        doctor.setTimeTable(timeTables);
        doctorDBInterface.save(doctor);
        Assert.assertNotNull(scenario.showAvailableAdmissionTime( "Surname"));
    }

    @After
    public void cleanUp() {
        DoctorRepository.doctors.clear();
        DoctorRepository.resetId();
        TimeTableRepository.timeTableMap.clear();
        TimeTableRepository.resetId();
        PatientRepository.patients.clear();
        PatientRepository.resetId();
        MedicalServiceRepository.medicalServiceMap.clear();
        MedicalServiceRepository.resetId();
        AvailableAdmissionTimeRepository.availableAdmissionTimeMap.clear();
        AvailableAdmissionTimeRepository.resetId();
        OrderedMedicalServiceRepository.orderedMedicalServiceMap.clear();
        OrderedMedicalServiceRepository.resetId();
    }

}

import logic.AvailableAdmissionTime;
import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;

public class AvailableAdmissionTimeTest {

    @Test(expected = IllegalArgumentException.class)
    public void constructorParseErrorException() {
        AvailableAdmissionTime availableAdmissionTime = new AvailableAdmissionTime("asd", "dsa", true, true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorEndTimeBeforeStartTime() {
        AvailableAdmissionTime availableAdmissionTime = new AvailableAdmissionTime("11:00", "10:00", true, true);
    }

    @Test
    public void constructorOk() {
        AvailableAdmissionTime availableAdmissionTime = new AvailableAdmissionTime("11:00", "12:00", true, true);
        Assert.assertNotNull(availableAdmissionTime);
    }
}

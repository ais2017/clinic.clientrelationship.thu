import UIs.Scenario;
import authority.User;
import db.*;
import logic.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import db.impl.fake.*;
import javax.persistence.NoResultException;
import javax.print.Doc;
import java.nio.file.AccessDeniedException;
import java.sql.Time;
import java.util.*;

/**
 * Created by anna on 14.12.2017.
 */
public class AuthenticationTest {

    private DoctorInterface doctorDBInterface = new DoctorRepository();
    private PatientInteface patientDBInterface = new PatientRepository();
    private TimeTableInterface timeTableDBInterface = new TimeTableRepository();
    private AvailableAdmissionTimeInterface availableAdmissionTimeDBInterface = new AvailableAdmissionTimeRepository();
    private MedicalServiceInterface medicalServiceDBInterface = new MedicalServiceRepository();
    private OrderedMedicalServiceInterface orderedMedicalServiceDBInterface = new OrderedMedicalServiceRepository();
    private UserInteface userInteface = new UserRepository();
    private DiseaseInteface diseaseInteface = new DiseaseRepository();


    @Test
    public void authPatientOk() throws AccessDeniedException {
        Scenario scenario = new Scenario(doctorDBInterface, patientDBInterface, timeTableDBInterface, availableAdmissionTimeDBInterface,
                medicalServiceDBInterface, orderedMedicalServiceDBInterface, userInteface, diseaseInteface);
        String policyNumber = "0123456789";
        Patient patient = new Patient(null, policyNumber, Application.NAME, Application.SURNAME, Application.PATRONYMIC, new ArrayList<>(), new ArrayList<>());
        patient = patientDBInterface.save(patient);
        scenario.authPatient(policyNumber);
        Assert.assertEquals(scenario.authPatient(policyNumber), true);
    }

    @Test(expected = AccessDeniedException.class)
    public void authPatientError() throws AccessDeniedException {
        Scenario scenario = new Scenario(doctorDBInterface, patientDBInterface, timeTableDBInterface, availableAdmissionTimeDBInterface,
                medicalServiceDBInterface, orderedMedicalServiceDBInterface, userInteface, diseaseInteface);
        String policyNumber = "1123456789";
        Patient patient = new Patient(null, policyNumber, Application.NAME, Application.SURNAME, Application.PATRONYMIC, new ArrayList<>(), new ArrayList<>());
        patient = patientDBInterface.save(patient);
        scenario.authPatient(policyNumber + "1");
    }

    @Test
    public void authStaffOk() throws AccessDeniedException {
        Scenario scenario = new Scenario(doctorDBInterface, patientDBInterface, timeTableDBInterface, availableAdmissionTimeDBInterface,
                medicalServiceDBInterface, orderedMedicalServiceDBInterface, userInteface, diseaseInteface);
        String login = "LOGIN";
        String password = "PASSWORD";
        User user = new User(login, password);
        userInteface.save(user);
        Assert.assertEquals(scenario.authStaff(login, password).getLogin(), user.getLogin());
    }

    @Test(expected = AccessDeniedException.class)
    public void authStaffError() throws AccessDeniedException {
        Scenario scenario = new Scenario(doctorDBInterface, patientDBInterface, timeTableDBInterface, availableAdmissionTimeDBInterface,
                medicalServiceDBInterface, orderedMedicalServiceDBInterface, userInteface, diseaseInteface);
        String login = "LOGIN";
        String password = "PASSWORD";
        User user = new User(login, password);
        userInteface.save(user);
        scenario.authStaff(login, password + "1");
    }

    @Test(expected = AccessDeniedException.class)
    public void authStaffError2() throws AccessDeniedException {
        Scenario scenario = new Scenario(doctorDBInterface, patientDBInterface, timeTableDBInterface, availableAdmissionTimeDBInterface,
                medicalServiceDBInterface, orderedMedicalServiceDBInterface, userInteface, diseaseInteface);
        String login = "LOGIN";
        String password = "PASSWORD";
        scenario.authStaff(login, password + "1");
    }

    @After
    public void cleanUp() {
        DoctorRepository.resetId();
        TimeTableRepository.timeTableMap.clear();
        TimeTableRepository.resetId();
        PatientRepository.patients.clear();
        PatientRepository.resetId();
        MedicalServiceRepository.medicalServiceMap.clear();
        MedicalServiceRepository.resetId();
        AvailableAdmissionTimeRepository.availableAdmissionTimeMap.clear();
        AvailableAdmissionTimeRepository.resetId();
        OrderedMedicalServiceRepository.orderedMedicalServiceMap.clear();
        OrderedMedicalServiceRepository.resetId();
    }
}

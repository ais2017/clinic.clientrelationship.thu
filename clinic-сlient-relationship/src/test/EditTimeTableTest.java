import db.*;
import logic.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import db.impl.fake.*;
import UIs.Scenario;
import javax.persistence.NoResultException;
import javax.print.Doc;
import java.sql.Time;
import java.util.*;

/**
 * Created by anna on 14.12.2017.
 */
public class EditTimeTableTest {

    private DoctorInterface doctorDBInterface = new DoctorRepository();
    private PatientInteface patientDBInterface = new PatientRepository();
    private TimeTableInterface timeTableDBInterface = new TimeTableRepository();
    private AvailableAdmissionTimeInterface availableAdmissionTimeDBInterface = new AvailableAdmissionTimeRepository();
    private MedicalServiceInterface medicalServiceDBInterface = new MedicalServiceRepository();
    private OrderedMedicalServiceInterface orderedMedicalServiceDBInterface = new OrderedMedicalServiceRepository();
    private UserInteface userInteface = new UserRepository();
    private DiseaseInteface diseaseInteface = new DiseaseRepository();


    @Test
    public void editTimeTable() {
        Scenario scenario = new Scenario(doctorDBInterface, patientDBInterface, timeTableDBInterface, availableAdmissionTimeDBInterface,
                medicalServiceDBInterface, orderedMedicalServiceDBInterface, userInteface, diseaseInteface);

        AvailableAdmissionTime availableAdmissionTime = availableAdmissionTimeDBInterface.generate("11:00", "12:00");
        availableAdmissionTime = availableAdmissionTimeDBInterface.save(availableAdmissionTime);
        TimeTable timeTable = timeTableDBInterface.generate(new Date());
        ArrayList<AvailableAdmissionTime> availableAdmissionTimes = new ArrayList<>();
        availableAdmissionTimes.add(availableAdmissionTime);
        timeTable.setAvailableAdmissionTimes(availableAdmissionTimes);
        Doctor doctor = doctorDBInterface.generate();
        ArrayList<TimeTable> timeTables = new ArrayList<>();
        timeTable = timeTableDBInterface.save(timeTable);
        timeTables.add(timeTable);
        doctor.setTimeTable(timeTables);
        doctorDBInterface.save(doctor);
        MedicalService medicalService = new MedicalService(null, "Name", 100, "Description");
        medicalServiceDBInterface.save(medicalService);

        Collection<AvailableAdmissionTime> availableAdmissionTimes1 = scenario.showAvailableAdmissionTime("Surname");
        Assert.assertEquals(availableAdmissionTimes1.size(), 1);
        Assert.assertEquals(availableAdmissionTimes1.iterator().next().getStartTime(), availableAdmissionTime.getStartTime());

        AvailableAdmissionTime availableAdmissionTimeNew = availableAdmissionTimeDBInterface.generate("12:00", "13:00");
        availableAdmissionTimeNew = availableAdmissionTimeDBInterface.save(availableAdmissionTime);
        ArrayList<AvailableAdmissionTime> availableAdmissionTimesNew = new ArrayList<>();
        availableAdmissionTimesNew.add(availableAdmissionTimeNew);
        scenario.editTimeTable("Surname", new Date(), availableAdmissionTimesNew);

        Collection<AvailableAdmissionTime> availableAdmissionTimes2 = scenario.showAvailableAdmissionTime("Surname");
        Assert.assertEquals(availableAdmissionTimes1.size(), 1);
        Assert.assertEquals(availableAdmissionTimes1.iterator().next().getStartTime(), availableAdmissionTimeNew.getStartTime());

    }

    @After
    public void cleanUp() {
        DoctorRepository.doctors.clear();
        DoctorRepository.resetId();
        TimeTableRepository.timeTableMap.clear();
        TimeTableRepository.resetId();
        PatientRepository.patients.clear();
        PatientRepository.resetId();
        MedicalServiceRepository.medicalServiceMap.clear();
        MedicalServiceRepository.resetId();
        AvailableAdmissionTimeRepository.availableAdmissionTimeMap.clear();
        AvailableAdmissionTimeRepository.resetId();
        OrderedMedicalServiceRepository.orderedMedicalServiceMap.clear();
        OrderedMedicalServiceRepository.resetId();
    }
}

import db.*;
import logic.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import db.impl.fake.*;
import UIs.Scenario;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class BlockPatientTest {

    private DoctorInterface doctorDBInterface = new DoctorRepository();
    private PatientInteface patientDBInterface = new PatientRepository();
    private TimeTableInterface timeTableDBInterface = new TimeTableRepository();
    private AvailableAdmissionTimeInterface availableAdmissionTimeDBInterface = new AvailableAdmissionTimeRepository();
    private MedicalServiceInterface medicalServiceDBInterface = new MedicalServiceRepository();
    private OrderedMedicalServiceInterface orderedMedicalServiceDBInterface = new OrderedMedicalServiceRepository();
    private UserInteface userInteface = new UserRepository();
    private DiseaseInteface diseaseInteface = new DiseaseRepository();

    @Before
    public void init() {
        Scenario scenario = new Scenario(doctorDBInterface, patientDBInterface, timeTableDBInterface, availableAdmissionTimeDBInterface,
                medicalServiceDBInterface, orderedMedicalServiceDBInterface, userInteface, diseaseInteface);
        Calendar myCal = Calendar.getInstance();
        myCal.set(Calendar.YEAR, 2017);
        myCal.set(Calendar.MONTH, Calendar.NOVEMBER);
        myCal.set(Calendar.DAY_OF_MONTH, 21);
        Date theDate = myCal.getTime();
        AvailableAdmissionTime availableAdmissionTime = availableAdmissionTimeDBInterface.generate("11:00", "12:00");
        TimeTable timeTable = timeTableDBInterface.generate(theDate);
        ArrayList<AvailableAdmissionTime> availableAdmissionTimes = new ArrayList<>();
        availableAdmissionTimes.add(availableAdmissionTime);
        timeTable.setAvailableAdmissionTimes(availableAdmissionTimes);
        Doctor doctor = doctorDBInterface.generate();
        ArrayList<TimeTable> timeTables = new ArrayList<>();
        timeTable = timeTableDBInterface.save(timeTable);
        timeTables.add(timeTable);
        doctor.setTimeTable(timeTables);
        doctorDBInterface.save(doctor);
        MedicalService medicalService = new MedicalService(null, "Name", 100, "Description");
        medicalServiceDBInterface.save(medicalService);
        Patient patient = new Patient(null, "1", "Name", "Surname", "Patronymic", new ArrayList<>(), new ArrayList<>());
        patient = patientDBInterface.save(patient);

        scenario.enroll(  "Surname", theDate, "11:00", "12:00", medicalService, patient);

    }

    @Test
    public void blockPatient() {
        Scenario scenario = new Scenario(doctorDBInterface, patientDBInterface, timeTableDBInterface, availableAdmissionTimeDBInterface,
                medicalServiceDBInterface, orderedMedicalServiceDBInterface, userInteface, diseaseInteface);
//        Assert.assertNotNull(OrderedMedicalServiceRepository.orderedMedicalServiceMap);
        scenario.blockPatient();
        Assert.assertNotNull(patientDBInterface.getAllBlocked());
    }

    @After
    public void cleanUp() {
        DoctorRepository.doctors.clear();
        DoctorRepository.resetId();
        TimeTableRepository.timeTableMap.clear();
        TimeTableRepository.resetId();
        PatientRepository.patients.clear();
        PatientRepository.resetId();
        MedicalServiceRepository.medicalServiceMap.clear();
        MedicalServiceRepository.resetId();
        AvailableAdmissionTimeRepository.availableAdmissionTimeMap.clear();
        AvailableAdmissionTimeRepository.resetId();
        OrderedMedicalServiceRepository.orderedMedicalServiceMap.clear();
        OrderedMedicalServiceRepository.resetId();
    }
}

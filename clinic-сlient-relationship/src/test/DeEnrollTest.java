import UIs.Scenario;
import db.*;
import logic.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import db.impl.fake.*;
import UIs.Scenario;
import javax.persistence.NoResultException;
import javax.print.Doc;
import java.sql.Time;
import java.util.*;

/**
 * Created by anna on 14.12.2017.
 */
public class DeEnrollTest {
    private DoctorInterface doctorDBInterface = new DoctorRepository();
    private PatientInteface patientDBInterface = new PatientRepository();
    private TimeTableInterface timeTableDBInterface = new TimeTableRepository();
    private AvailableAdmissionTimeInterface availableAdmissionTimeDBInterface = new AvailableAdmissionTimeRepository();
    private MedicalServiceInterface medicalServiceDBInterface = new MedicalServiceRepository();
    private OrderedMedicalServiceInterface orderedMedicalServiceDBInterface = new OrderedMedicalServiceRepository();
    private UserInteface userInteface = new UserRepository();
    private DiseaseInteface diseaseInteface = new DiseaseRepository();


    @Test
    public void deEnroll() {
        Scenario scenario = new Scenario(doctorDBInterface, patientDBInterface, timeTableDBInterface, availableAdmissionTimeDBInterface,
                medicalServiceDBInterface, orderedMedicalServiceDBInterface, userInteface, diseaseInteface);
        AvailableAdmissionTime availableAdmissionTime = availableAdmissionTimeDBInterface.generate("11:00", "12:00");
        TimeTable timeTable = timeTableDBInterface.generate(new Date());
        ArrayList<AvailableAdmissionTime> availableAdmissionTimes = new ArrayList<>();
        availableAdmissionTimes.add(availableAdmissionTime);
        timeTable.setAvailableAdmissionTimes(availableAdmissionTimes);
        Doctor doctor = doctorDBInterface.generate();
        ArrayList<TimeTable> timeTables = new ArrayList<>();
        timeTable = timeTableDBInterface.save(timeTable);
        timeTables.add(timeTable);
        doctor.setTimeTable(timeTables);
        doctorDBInterface.save(doctor);
        MedicalService medicalService = new MedicalService(null, "Name", 100, "Description");
        medicalServiceDBInterface.save(medicalService);
        Patient patient = new Patient(null, "1", "Name", "Surname", "Patronymic", new ArrayList<>(), new ArrayList<>());
        patient = patientDBInterface.save(patient);

        scenario.enroll("Surname", new Date(), "11:00", "12:00", medicalService, patient);
        //Создалась заказанная услуга
        Assert.assertNotNull(OrderedMedicalServiceRepository.orderedMedicalServiceMap);

        scenario.deEnroll("Surname", OrderedMedicalServiceRepository.orderedMedicalServiceMap.get(1L), patient);

        //Удалилась заказанная услуга
        Assert.assertEquals(OrderedMedicalServiceRepository.orderedMedicalServiceMap.size(), 0);

        //Появилось время для записи
        Assert.assertNotNull(scenario.showAvailableAdmissionTime( "Surname"));
    }

    @After
    public void cleanUp() {
        DoctorRepository.doctors.clear();
        DoctorRepository.resetId();
        TimeTableRepository.timeTableMap.clear();
        TimeTableRepository.resetId();
        PatientRepository.patients.clear();
        PatientRepository.resetId();
        MedicalServiceRepository.medicalServiceMap.clear();
        MedicalServiceRepository.resetId();
        AvailableAdmissionTimeRepository.availableAdmissionTimeMap.clear();
        AvailableAdmissionTimeRepository.resetId();
        OrderedMedicalServiceRepository.orderedMedicalServiceMap.clear();
        OrderedMedicalServiceRepository.resetId();
    }
}

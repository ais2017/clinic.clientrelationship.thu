import db.*;
import logic.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import db.impl.fake.*;
import UIs.Scenario;
import javax.persistence.NoResultException;
import javax.print.Doc;
import java.sql.Time;
import java.util.*;

public class PayForServiceTest {

    private DoctorInterface doctorDBInterface = new DoctorRepository();
    private PatientInteface patientDBInterface = new PatientRepository();
    private TimeTableInterface timeTableDBInterface = new TimeTableRepository();
    private AvailableAdmissionTimeInterface availableAdmissionTimeDBInterface = new AvailableAdmissionTimeRepository();
    private MedicalServiceInterface medicalServiceDBInterface = new MedicalServiceRepository();
    private OrderedMedicalServiceInterface orderedMedicalServiceDBInterface = new OrderedMedicalServiceRepository();
    private UserInteface userInteface = new UserRepository();
    private DiseaseInteface diseaseInteface = new DiseaseRepository();

    @Before
    public void init() {

    }

    @Test
    public void payForService() {
        Scenario scenario = new Scenario(doctorDBInterface, patientDBInterface, timeTableDBInterface, availableAdmissionTimeDBInterface,
                medicalServiceDBInterface, orderedMedicalServiceDBInterface, userInteface, diseaseInteface);
        AvailableAdmissionTime availableAdmissionTime = availableAdmissionTimeDBInterface.generate("11:00", "12:00");
        AvailableAdmissionTime availableAdmissionTime2 = availableAdmissionTimeDBInterface.generate("12:00", "13:00");

        TimeTable timeTable = timeTableDBInterface.generate(new Date());
        ArrayList<AvailableAdmissionTime> availableAdmissionTimes = new ArrayList<>();
        availableAdmissionTimes.add(availableAdmissionTime);
        availableAdmissionTimes.add(availableAdmissionTime2);
        timeTable.setAvailableAdmissionTimes(availableAdmissionTimes);
        Doctor doctor = doctorDBInterface.generate();
        ArrayList<TimeTable> timeTables = new ArrayList<>();
        timeTable = timeTableDBInterface.save(timeTable);
        timeTables.add(timeTable);
        doctor.setTimeTable(timeTables);
        doctorDBInterface.save(doctor);
        MedicalService medicalService = new MedicalService(null, "Name", 100, "Description");
        medicalService = medicalServiceDBInterface.save(medicalService);
        MedicalService medicalService1 = medicalServiceDBInterface.save(medicalService);
        Patient patient = new Patient(null, "1", "Name", "Surname", "Patronymic", new ArrayList<>(), new ArrayList<>());
        patient = patientDBInterface.save(patient);
        scenario.enroll( "Surname", new Date(),
                "11:00", "12:00", medicalService, patient);
        scenario.enroll( "Surname", new Date(),
                "12:00", "13:00", medicalService1, patient);

        Assert.assertEquals(scenario.payForService( patient.getPolicyNumber(), 200).iterator().next().isPaid(), true);

    }

    @After
    public void cleanUp() {
        DoctorRepository.doctors.clear();
        DoctorRepository.resetId();
        TimeTableRepository.timeTableMap.clear();
        TimeTableRepository.resetId();
        PatientRepository.patients.clear();
        PatientRepository.resetId();
        MedicalServiceRepository.medicalServiceMap.clear();
        MedicalServiceRepository.resetId();
        AvailableAdmissionTimeRepository.availableAdmissionTimeMap.clear();
        AvailableAdmissionTimeRepository.resetId();
        OrderedMedicalServiceRepository.orderedMedicalServiceMap.clear();
        OrderedMedicalServiceRepository.resetId();
    }
}

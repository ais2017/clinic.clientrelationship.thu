# Описание организации
Медицинское учреждение, осуществляющее амбулаторный прием пациентов. В клинику могут поступать пациенты с состоянием различной степени тяжести. Прием осуществляется по предварительной записи, либо по факту прихода, в зависимости от состояния пациента. Пациент может быть принят по страховке, либо расплатиться за услуги самостоятельно после их оказания.
# Описание области автоматизации
Система должна автоматизировать процессы регистрации пациентов и оплаты оказанных услуг.